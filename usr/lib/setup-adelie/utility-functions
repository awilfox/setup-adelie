#!/bin/sh
# Copyright (C) 2018 Elizabeth Myers. All rights reserved.
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

# These are useful utility functions/overrides for builtin stuff.

# echo: a safer replacement for echo
# Semantics are like bash's echo
# See http://www.etalabs.net/sh_tricks.html
echo ()
(
	fmt=%s end=\\n IFS=" "

	while [ $# -gt 1 ] ; do
		case "$1" in
		[!-]*|-*[!ne]*) break ;;
		*ne*|*en*) fmt=%b end= ;;
		*n*) end= ;;
		*e*) fmt=%b ;;
		esac
		shift
	done

	printf "$fmt$end" "$*"
)

# quote: Properly quote variables for use in eval etc.
# Shamelessly stolen from http://www.etalabs.net/sh_tricks.html
#
# Arguments:
# $1: text to quote
#
# Side effects:
# Outputs quoted variable
quote ()
{
	printf %s\\n "$1" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/'/"
}

# save_array: Saves array for use later
# Shamelessly stolen from http://www.etalabs.net/sh_tricks.html
#
# Basically POSIX shell doesn't have arrays and we hack it with $@. It works,
# but we have to save the previous values and set them later with something
# like eval "set -- $values". This saves the values for use in that.
#
# Arguments:
# $@: arguments you want to save for later
#
# Side effects:
# Outputs quoted strings of arguments to save
save_array ()
{
	for i do
		printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"
	done
	echo " "
}

# read_heredoc: POSIX way of reading from a heredoc
# Stolen from somewhere on stackoverflow and lightly modified
#
# Arguments:
# $1: variable to save to
#
# Side effects:
# None
read_heredoc ()
{
	_NL="
"
	_STR=""
	_IFS_OLD="$IFS"
	IFS="$_NL"
	while read -r _LINE; do
		_STR="${_STR}${_LINE}${_NL}"
	done
	IFS=${_IFS_OLD}
	eval "$1"="\${_STR}"
	unset _STR _IFS_OLD
}

# error: kill the script
#
# Arguments:
# $1: error text
#
# Side effects:
# Exits the script
error ()
{
	dialog_std \
		--title "Error" \
		--msgbox "$1" \
		0 0
	reset
	exit 1 
}

# convert_date: Convert a date to standard format
#
# Arguments:
# $1: date format
# $2: date string to convert
# $3: optional datetime string, defaults to $STD_DATETIME_FMT
#
# Side effects:
# None
convert_date ()
(
	if [ -n "$3" ]; then
		FMT="$3"
	else
		FMT="$STD_DATETIME_FMT"
	fi

	# date format is ignored with GNU date
	$CMD_DATE --date="$2" +"$FMT"
)
