# setup-adelie

This is a basic installer for Adélie Linux, codenamed "High Noon."

## Prerequisites
The installer requires the following utilities and/or packages:

* A POSIX-compatible shell
* `apk`
* A GNU compliant `date`
* `dhcpcd`
* `dialog` or compatible program
* `$EDITOR` to be set (or `vim`)
* `ip`
* `lsblk`
* `mkfs.xfs` and `mkfs.ext4`
* `parted`
* `tzdata` (for `/usr/share/zoneinfo/`)

With a bit of work, many of these requirements are flexible, except a POSIX-compliant
shell.

## Assumptions
* You have 3GB or more of disk space
* You are using a supported Adélie Linux architecture
* This is actually Adélie Linux you're installing (Alpine may work but it's not supported)
* No aliases are set (just don't do it)

## Code style
* Tab indents, no spaces; the way it's meant to be
* Limit lines to 80 columns
* No shell constructs that aren't POSIX, except for non-POSIX commands when needed
* Unless you have a good reason, use `dialog` for all UI elements
* Use subshells unless you have good reason (example: `fn () ( ... )` instead of `fn () { ... }`

## Bugs
* More than an entomologist's office, no doubt
* This is basically a prototype that will hopefully not live too long, or at least be rewritten
* Utility error outputs should be redirected to a log file
* Needs more debugging instrumentation (see also: logging)
* It's in shell, which is a colossal bug in of itself
* It's all very blue, because `dialog` is very blue; perhaps we should make a dialogrc that is less blue
* The manual config flows really suck right now
* Wireless network config
* LVM/PV/RAID needed for disks
* Better automatic fstab generation
