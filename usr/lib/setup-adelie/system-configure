#!/bin/sh
# Copyright (C) 2018 Elizabeth Myers. All rights reserved.
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

# System configuration functions

# config_skeleton: Set up basic directories for installation
#
# Arguments:
# None
#
# Side effects:
# Makes directories in target, mounts appropriate fses
config_skeleton ()
(
	for DIR in boot/grub etc/conf.d etc/apk proc sys run dev tmp; do
		mkdir -p "${TARGET}/${DIR}"
	done

	mount -t proc proc "${TARGET}/proc"
	mount -t sysfs sysfs "${TARGET}/sys"
	mount -t tmpfs tmpfs "${TARGET}/run"
	mount -t tmpfs tmpfs "${TARGET}/tmp"
	mount -B /dev "${TARGET}/dev"
)

# config_disk_generic: Generically set up a partitioned disk
#
# Arguments:
# $1: boot partition
# $2: root partition
# $3: swap partition
# $4+: additional partitions in the format "device mountpoint fs", all will
#      be mounted
#
# Side effects:
# Filesystems created, fstab written, root and boot mounted
config_disk_generic ()
(
	# Unmount any fses in the target
	umount -R "${TARGET}" &>/dev/null

	mkfs -t ext4 -F -L boot $1 &>/dev/null || \
		error "Could not make boot fs"
	mkfs -t xfs -f -L root $2 &>/dev/null || \
		error "Could not make root fs"

	mount -t xfs $2 "${TARGET}" &>/dev/null || \
		error "Could not mount root fs"

	mkdir -p "${TARGET}/boot"

	mount -t ext4 $1 "${TARGET}/boot" || \
		error "Could not mount boot fs"

	cat >"${TARGET}/etc/fstab" <<EOF
# This fstab was generated automatically by setup-adelie

EOF

	# XXX this is all super ugly!
	ENTRIES="#device mountpoint fs options dump fsck"
	ENTRIES="${ENTRIES}\n${1} /boot ext4 defaults 0 1"
	ENTRIES="${ENTRIES}\n${2} / xfs defaults 1 1"

	if [ -n "$3" ]; then
		mkswap $3
		ENTRIES="${ENTRIES}\n${3} swap swap defaults 0 0"
	fi

	shift 3
	while [ -n "$@" ]; do
		SAVED=$(save_array "$@")
		set -- $1

		DEVICE="$1"
		MOUNTPOINT="$2"
		FS="$3"

		mkfs -t $FS $DEVICE &>/dev/null || \
			error "Couldn't make auxillary FS $DEVICE"

		ENTRIES="${ENTRIES}\n${DEVICE} ${MOUNTPOINT} ${FS} defaults 0 0"
		mount -t $FS $DEVICE "${TARGET}/${MOUNTPOINT}"
		eval "set -- $SAVED"
		shift
	done

	config_skeleton

	# Write back the fstab
	echo "${ENTRIES}" | column -t >>"${TARGET}/etc/fstab"
)

# config_disk_auto_x86_mbr: Automatically partition a disk, x86 style
#
# Arguments:
# $1: disk to mangle
# $2: whether or not to use swap
#
# Side effects:
# Disk is mangled (and partitioned) with a 512MiB boot, adjustable swap, and /
config_disk_auto_x86_mbr ()
(
	DEVICE="/dev/$1"
	PART_BOOT="${DEVICE}1"
	PART_ROOT="${DEVICE}2"
	PART_SWAP=""

	$CMD_PARTED -s -a optimal $DEVICE mklabel msdos 2>/dev/null || \
		error "Could not create disk label"

	# Boot
	$CMD_PARTED -s -a optimal $DEVICE mkpart primary ext4 1MiB 512MiB \
		2>/dev/null

	# Swap?
	if [ $2 -eq 1 ]; then
		DEVICE_SPACE=$(get_device_space $DEVICE)
		SWAPSIZE=$(ask_swap $DEVICE_SPACE 513) # Reserve /boot and 1MiB

		# 512 MiB is boot
		SWAP_OFFSET="$(($DEVICE_SPACE - $SWAPSIZE + 513))MiB"

		$CMD_PARTED -s -a optimal $DEVICE mkpart primary xfs 512MiB \
			$SWAP_OFFSET
		$CMD_PARTED -s -a optimal $DEVICE mkpart primary swap \
			$SWAP_OFFSET -0

		PART_SWAP="/dev/${1}3"
	else
		$CMD_PARTED -s -a optimal $DEVICE mkpart primary xfs 512MiB -0
	fi
	
	config_disk_generic ${PART_BOOT} ${PART_ROOT} ${PART_SWAP} || \
		error "Could not set up disk"
)

# config_disk_auto_x86_efi: Automatically partition a disk, EFI style
#
# Arguments:
# $1: disk to mangle
# $2: whether or not to use swap
#
# Side effects:
# Disk is mangled (and partitioned) with a 250 MiB ESP, 512MiB boot, adjustable
# swap, and /
config_disk_auto_x86_efi ()
(
	DEVICE="/dev/$1"
	PART_ESP="${DEVICE}1"
	PART_BOOT="${DEVICE}2"
	PART_ROOT="${DEVICE}3"
	PART_SWAP=""

	$CMD_PARTED -s -a optimal $DEVICE mklabel gpt 2>/dev/null || \
		error "Could not create disk label"
	
	# ESP (250 MiB, seems sensible)
	$CMD_PARTED -s -a optimal $DEVICE mkpart ESP fat32 1MiB 251MiB \
		set 1 esp on 2>/dev/null

	# Boot (512 MiB)
	$CMD_PARTED -s -a optimal $DEVICE mkpart primary boot 251MiB 763MiB \
		2>/dev/null

	# Swap?
	if [ $2 -eq 1 ]; then
		DEVICE_SPACE=$(get_device_space $DEVICE)
		SWAPSIZE=$(ask_swap $DEVICE_SPACE 763) # Reserve /boot and ESP

		SWAP_OFFSET="$(($DEVICE_SPACE - $SWAPSIZE + 763))MiB"

		$CMD_PARTED -s -a optimal $DEVICE mkpart primary xfs 763MiB \
			$SWAP_OFFSET
		$CMD_PARTED -s -a optimal $DEVICE mkpart primary swap \
			$SWAP_OFFSET -0

		PART_SWAP="/dev/${1}3"
	else
		$CMD_PARTED -s -a optimal $DEVICE mkpart primary xfs 763MiB -0
	fi
	
	EXTRA_PART_ESP="${PART_ESP} /boot/efi fat32"
	config_disk_generic ${PART_BOOT} ${PART_ROOT} ${PART_SWAP} \
		"${EXTRA_PART_ESP}" || \
		error "Could not set up disk"
)

# config_disk_auto: Call the correct function to set up the disk
#
# Arguments:
# $1: disk to mangle
# $2: whether or not to use swap
#
# Side effects:
# Same as the function that is called
config_disk_auto ()
(
	case $(uname -m) in
	x86|i[3-6]86|x86_64)
		# Probe for EFI
		if [ -d /sys/firmware/efi/efivars ]; then
			config_disk_auto_x86_efi $1 $2 || exit 1
		else
			config_disk_auto_x86_mbr $1 $2 || exit 1
		fi
	;;
	*)
		exit 2
	;;
	esac
)

# config_timezone: Configure the user's timezone
#
# Arguments:
# $1: timezone in zoneinfo format
#
# Side effects:
# Removes any old zoneinfo files, creates new symlinks
config_timezone ()
(
	# Adélie uses /usr/share/zoneinfo
	rm -f /etc/zoneinfo
	ln -sf "/usr/share/zoneinfo/${1}" /etc/zoneinfo

	rm -f ${TARGET}/etc/zoneinfo
	ln -sf "/usr/share/zoneinfo/${1}" "${TARGET}/etc/zoneinfo"

	export TZ="${1}"
)

# config_date: Configure the date and time
#
# Arguments:
# $1: Time format
# $2: Actual time to set to
#
# Side effects:
# Timezone is set
config_date ()
{
	$CMD_DATE +"$1" -s "$2" 2>/dev/null || error "Could not set time"
}

# config_iface_dhcp: Set up an interface for DHCP
#
# Arguments:
# $1: interface
#
# Side effects:
# Writes to network config file
config_iface_dhcp ()
(
	cat >>"$TARGET/etc/conf.d/net" <<EOF

# ===BEGIN GENERATED CONFIG FOR $1===
config_$1="dhcp"
# ===END GENERATED CONFIG FOR $1===

EOF

	[ -d "${TARGET}/etc/init.d" ] || mkdir -p "${TARGET}/etc/init.d"
	ln -rs "${TARGET}/etc/init.d/net.lo" \
		"${TARGET}/etc/init.d/net.${1}" || \
		error "Could not write network files"

	if [ ! -f /run/dhcpcd-$1.pid ]; then
		$CMD_DHCPCD $1 || error "Could not set up DHCP on $1"
	fi
)

# config_iface_static: Set up an interface with a static IP
#
# Arguments:
# $1: interface
# $2: IP
# $3: default route
# $4: DNS search domain
# $5: DNS servers
#
# Side effects:
# Writes to network config file
#
# Bugs:
# Only handles IPv4 atm
config_iface_static ()
(
	cat >>"$TARGET/etc/conf.d/net" <<EOF

# ===BEGIN GENERATED CONFIG FOR $1==
config_$1="$2"
routes_$1="default via $3"
dns_domain_$1="$4"
dns_servers_$1="$5"
# ===END GENERATED CONFIG FOR $1===

EOF

	[ -d "${TARGET}/etc/init.d" ] || mkdir -p "${TARGET}/etc/init.d"
	ln -rs "${TARGET}/etc/init.d/net.lo" \
		"${TARGET}/etc/init.d/net.${1}" || \
		error "Could not write network files"

	$CMD_IP addr flush dev $1 2>/dev/null
	$CMD_IP route flush dev $1 2>/dev/null

	$CMD_IP link set $1 up 2>/dev/null ||
		error "Could not bring up interface $1"
	$CMD_IP addr add $2 dev $1 2>/dev/null || \
		error "Could not assign IP $2 to $1"
	$CMD_IP route add default via $3 dev $1 2>/dev/null || \
		error "Could not assign default route $3"

	if [ -n "$4" ]; then
		echo "search $4" >>/etc/resolv.conf
	fi

	if [ -n "$5" ]; then
		for SERV in $5; do
			echo "nameserver $SERV" >>/etc/resolv.conf
		done
	fi
)

# config_hostname: Set the system's hostname
#
# Arguments:
# $1: System hostname
#
# Side effects:
# Sets system hostname
config_hostname ()
(
	hostname "$1" 2>/dev/null || error "Could not set hostname"
	echo "$1" > "${TARGET}/etc/hostname"
)

# config_base: Configure base system in the target
#
# Arguments:
# None
#
# Side effects:
# Base system is prepared in the target
config_base ()
(
	echo "Initalising APK..."
	apk --root "${TARGET}" --initdb add || exit 1
	cp -r /etc/apk/keys "${TARGET}/etc/apk/"
	cp /etc/apk/repositories "${TARGET}/etc/apk/"

	echo
	echo "Installing base system..."
	# Current recommended defaults
	apk --root "${TARGET}" add adelie-base bash-binsh ssmtp openrc \
		sysvinit easy-kernel easy-kernel-modules netifrc || \
		exit 1

	cp -PRr "${TARGET}/usr/share/openrc/runlevels" \
		"${TARGET}/etc/runlevels"
	cp -p /etc/shells "${TARGET}/etc/"

	# XXX these paths may have to be absolute
	echo
	echo "Adding essential services to startup..."
	chroot "${TARGET}" rc-update add udev boot
	chroot "${TARGET}" rc-update add udev-trigger boot
)

# config_bootloader: Install the bootloader
#
# Arguments:
# $1: disk to install it on
#
# Side effects:
# Bootloader is installed
#
# Bugs:
# Only grub is supported
config_bootloader ()
(
	# Common to all arches
	ROOT=$(mount | awk '/\/target[ \t]/ { print $1 }')
	if [ -z "$ROOT" ]; then
		exit 1
	fi

	cat >"${TARGET}/etc/update-extlinux.conf" <<EOF
# This file was generated by setup-adelie
default_kernel_opts=ro	# Any kernel options you need
modules=		# Any modules you need
root=${ROOT}		# Your root device here
GRUB_DISTRIBUTOR=Adelie
EOF

	case $(uname -m) in
	x86|i[3-6]86|x86_64)
		# FIXME efi
		# XXX these paths may have to be absolute
		apk add --root "${TARGET}" grub grub-bios || exit 1
		mkdir -p "${TARGET}/boot/grub" || exit 1
		chroot "${TARGET}" grub-mkconfig -o /boot/grub/grub.cfg || exit 1
		chroot "${TARGET}" grub-install --boot-directory=/boot "/dev/${1}" || \
			exit 1
	;;
	ppc|ppc64)
		# Cargo culted from Adélie Wiki
		# FIXME talos
		# XXX these paths may have to be absolute
		apk add --root "${TARGET}" grub grub-ieee1275 || exit 1
		mkdir -p "${TARGET}/boot/grub" || exit 1
		chroot "${TARGET}" grub-mkconfig -o /boot/grub/grub.cfg || exit 1
		chroot "${TARGET}" grub-install /boot/grub || exit 1
		chroot "${TARGET}" grub-macbless /boot/grub/grub || exit 1
		# XXX magic URL
		curl -o "${TARGET}/boot/grub/ofboot.b" \
			https://distfiles.adelielinux.org/source/grub-ofboot.b || \
			exit 1
	;;
	*)
		exit 2
	;;
	esac
)
