#!/bin/sh
# Copyright (C) 2018 Elizabeth Myers. All rights reserved.
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

# This script assumes an Adélie Linux install CD as the environment. Most of
# these assumptions are hardcoded and not easily fixed. This is probably fine
# for the forseeable future, but this script will not be suitable for something
# like Adélie BSD. I hope this is replaced by then.

################################################################################
# Bootstrapping code

# You shouldn't have any aliases set on the livecd, dingus.
unalias -a

# This method was taken from https://unix.stackexchange.com/a/76518
scriptpath ()
(
	exec 2>/dev/null
	cd -- $(dirname "$0")
	unset PWD
	pwd
)

abspath ()
(
	cd "$(dirname "$1")"
	printf "%s/%s\n" "$(pwd)" "$(basename "$1")"
)

SCRIPT_PATH=$(scriptpath)
LIB_PATH=$(abspath "${SCRIPT_PATH}/../lib/setup-adelie/")
################################################################################

# Load libraries
. ${LIB_PATH}/constants
. ${LIB_PATH}/paths
. ${LIB_PATH}/adelie-info
. ${LIB_PATH}/utility-functions
. ${LIB_PATH}/system-probe
. ${LIB_PATH}/system-configure
. ${LIB_PATH}/dialog

# Useful globals
COUNTRY=""

# do_exit: get the hell out of Dodge
#
# Arguments:
# $1: exit code
#
# Side effects:
# target is recursively unmounted, other cleanup
do_exit ()
{
	umount -R "${TARGET}" &>/dev/null
	exit $1
}

# ask_swap: Get swap space amount
#
# Arguments:
# $1: device to put swap on
# $2: device space
# $3: amount of reserved space (3GB will be added on)
#
# Side effects:
# None
ask_swap ()
(
	DEVICE=$1
	RAWSIZE=$2
	RESERVED=$3

	if [ -z "$RESERVED" ]; then
		RESERVED=0
	fi

	# Add reservation
	RESERVED=$(($RESERVED + 3072))

	SPACE=$(($RAWSIZE - $RESERVED))

	# Recommended swap is 2x free memory
	RECOMMENDED=$(($(get_system_mem) * 2))

	# This case is unlikely but handle it anyway
	if [ $RECOMMENDED -ge $SPACE ]; then
		RECOMMENDED=$(($SPACE / 4))
	fi

	dialog_read SWAP \
		--title "Automatic Disk Configuration" \
		--nocancel \
		--rangebox "Amount of swap space in mebibytes:" \
		0 80 1 $SPACE $SWAPMEM

	if [ -z "$SWAP" ]; then
		SWAP=$SWAPMEM
	fi

	echo "$SWAP"
)

# gen_country_args: Generate args for menu
# This function is internal
_gen_country_args ()
(
	COUNTRY_DB="$1"
	LINETOTAL=$(echo "$COUNTRY_DB" | wc -l | sed 's:[ \t]*::g')

	exec 4>/dev/tty
	exec 5>&1

	I=0
	IFS=$(echo -e '\t\n')
	echo "$COUNTRY_DB" | while read -r ISOCODE NAME; do
		ISOCODE=$(quote "$ISOCODE")
		NAME=$(quote "$NAME")
		echo -n "$ISOCODE $NAME " >&5

		I=$(($I + 1))
		echo $(($I * 100 / $LINETOTAL))
	done | (dialog_std \
		--title "Loading" \
		--gauge "Loading country names" \
		0 0 >&4)

	exec 4>&-
	exec 5>&-
)

# ask_country: Ask for the user's country
#
# Arguments:
# None
#
# Side effects:
# None
ask_country ()
(
	FILE="${ZONEINFO_PATH}/iso3166.tab"

	[ -f "$FILE" ] || error "Could not find tzdata"

	COUNTRY_DB=$(grep -Ev '^#' "$FILE")
	ARGS=$(_gen_country_args "$COUNTRY_DB")
	
	exec 4>/dev/tty

	SAVED=$(save_array "$@")
	eval "set -- $ARGS"
	dialog_read COUNTRY \
		--title "Country selection" \
		--menu "Select your country:" \
		20 60 15 \
		"$@" >&4
	eval "set -- $SAVED"

	exec 4>&-

	echo "$COUNTRY"
)

# setup_intro: Display the opening text
#
# Arguments:
# None
#
# Side effects:
# May exit the script if the user chooses "no"
setup_intro ()
(
	read_heredoc INTRO_TEXT <<EOF
This is ${ADELIE_PRETTY_NAME}.

This installer will help you configure the disk, network, system time, and
mirrors.

You may configure other packages after the system is installed.

Do you wish to continue with installation?
EOF

	dialog_std \
		--title "Adélie Linux Installation" \
		--yesno "$INTRO_TEXT" \
		0 0
	exit $?
)

# setup_country: Set up the country and keymap
#
# Arguments:
# None
#
# Side effects:
# COUNTRY is set
setup_country ()
{
	COUNTRY=$(ask_country)
}

# setup_disks: Set up disk and mount our target
#
# Arguments:
# None
#
# Side effects:
# May create new partition table or FSes, mounts new /
setup_disks ()
(
	while true; do
		dialog_read SELECTION \
			--title "Disk Configuration" \
			--menu "Please select one of the following:" \
			0 0 0 \
			"1" "List disk information" \
			"2" "Manually partition" \
			"3" "Exit"
			#"2" "Partition a disk automatically" \
		EXIT_CODE=$?
		case $? in
		1)
			exit 1
		;;
		255)
			exit
		;;
		esac

		case $SELECTION in
		1)
			DISKS=$(get_disks_list)
			dialog_std \
				--title "Disk information" \
				--no-collapse \
				--msgbox "$DISKS" \
				0 0
		;;
#		2)
			# Automatic mode
#			DISKS=$(get_disks)
#
			# Build args list
#			ARGS=""
#			I=1
#			for DISK in DISKS; do
#				DISK=$(quote "$DISK")
#				ARGS="$ARGS $I $DISK"
#				I=$(($I + 1))
#			done
#
#			SAVED=$(save_array "$@")
#			eval "set -- $ARGS"
#			dialog_read DISK \
#				--title "Automatic Disk Configuration" \
#				--menu "Select a disk to install onto:" \
#				0 0 0 \
#				"$@"
#			eval "set -- $SAVED"
#
#			if [ $? -eq 1 -o $? -eq -1 ]; then
#				continue
#			fi
#
#			read_heredoc DIALOG_TEXT <<EOF
#Are you SURE you wish to proceed?
#
#$DISK will be PERMANENTLY erased!
#
#Repeat: This operation cannot be undone!
#EOF
#			dialog_std \
#				--title "Automatic Disk Configuration" \
#				--defaultno \
#				--yesno "$DIALOG_TEXT" \
#				0 0
#			case $? in
#			1|255)
#				continue
#			;;
#			esac
#
#			config_disk_auto $DISK
#			case $? in
#			1)
#				# Regular error
#				exit 1
#			;;
#			2)
#				# Unsupported architecture
#				read_heredoc DIALOG_TEXT <<EOF
#Sorry, we don't yet support automatic partitioning on $(uname -m).
#
#You will have to do it manually.
#EOF
#				dialog_std \
#					--title "Error" \
#					--msgbox "$DIALOG_TEXT" \
#					0 0
#			;;
#			*)
#				exit
#			;;
#			esac
#		;;
		2)
			# Manual mode
			read_heredoc DIALOG_TEXT <<EOF
You are about to enter manual partitioning mode, which will drop you to a shell
to set up your disks the way you want them. You can use parted, lvm, or any
combination your heart desires.

You will need to manually mount your desired root at ${TARGET}.  For example:

# mount /dev/sda2 ${TARGET}

 or

# mount /dev/volumegroup/root ${TARGET}

You will need to mount any partitions you intend to use in your final install,
such as /home and /boot, inside ${TARGET}.

You will additionally need to write an /etc/fstab in ${TARGET}.

Once you are finished, exit the shell to proceed with installation.

Are you sure you want to continue?
EOF
			dialog_std \
				--title "Manual partitioning" \
				--defaultno \
				--yesno "$DIALOG_TEXT" \
				0 0

			if [ $? -eq 0 ]; then
				clear
				sh
			fi

			if [ -z "$(mount | grep "${TARGET}")" ]; then
				dialog_std \
					--title "Uhh..." \
					--ok-label "Fine, I'll fix it" \
					--msgbox "You didn't set anything up" \
					5 31
			else
				local _foo
				for _foo in etc proc sys dev; do
					mkdir -p "${TARGET}/$_foo"
				done
				# help the user out, mount the fun stuff for them if they didn't
				if [ -z "$(mount | grep "${TARGET}/proc")" ]; then
					mount -t proc none "${TARGET}/proc"
				fi
				if [ -z "$(mount | grep "${TARGET}/sys")" ]; then
					mount -t sysfs none "${TARGET}/sys"
				fi
				if [ -z "$(mount | grep "${TARGET}/dev")" ]; then
					mount -B /dev "${TARGET}/dev"
				fi
				if [ -z "$(mount | grep "${TARGET}/dev/pts")" ]; then
					mount -B /dev/pts "${TARGET}/dev/pts"
				fi
			fi
		;;
		3)
			# Normal exit
			exit
		;;
		*)
			exit 1
		;;
		esac
	done
)

# setup_timezone: Set the current timezone
#
# Arguments:
# None
#
# Side effects:
# Timezone is set
#
# Notes:
# Needs COUNTRY to be set
setup_timezone ()
(
	ENTRIES=$(grep -E "^${COUNTRY}" "${ZONEINFO_PATH}/zone.tab" | \
		cut -f3 | sort -t '/')
	ENTRIES=$(echo -e "$ENTRIES\nUTC\nOther")

	# We have to do the below crap to avoid an unwanted subshell
	ARGS=""
	IFS=$(echo -e "\t\n")
	while read -r TZ; do
		TZ=$(quote "$TZ")
		ARGS="$ARGS $TZ $TZ"
		I=$(($I + 1))
	done <<EOF
$ENTRIES
EOF

	SAVED=$(save_array "$@")
	eval "set -- $ARGS"
	dialog_read TZ \
		--title "Timezone selection" \
		--no-tags \
		--menu "Select your timezone:" \
		0 0 0 \
		"$@"
	EXIT_CODE=$?
	eval "set -- $SAVED"

	case $EXIT_CODE in 1|255) exit ;; esac

	# Nothing left to do
	if [ -f "${ZONEINFO_PATH}/${TZ}" ]; then
		config_timezone "$TZ"
		exit
	fi

	# "Other" was selected or something
	TZ=""
	while true; do
		dialog_read TZ \
			--title "Timezone input" \
			--nocancel \
			--inputbox "Input timezone (Unix format):" \
			0 0
		case $? in 1|255) exit; ;; esac

		# Replace "'s, .'s, and \'s, with nothing, remove duplicate
		# slashes, remove leading slash if any, and replace all spaces
		# and -'s with underscores. This should sanitise things enough.
		TZ=$(echo "$TZ" | \
			sed 's:[\"\.\\]::g;s:\(/\)\1*:\1:g;s:^/::g;s:[ -]:_:g')

		if [ ! -f "${ZONEINFO_PATH}/${TZ}" ]; then
			dialog_std \
				--title "Error" \
				--msgbox "Invalid timezone" \
				5 20
		else
			break
		fi
	done

	config_timezone "$TZ"
)

# setup_date_time: Set the current date and time
#
# Arguments:
# None
#
# Side effects:
# Date and time are set
setup_date_time ()
(
	DATE=$($CMD_DATE +"$STD_DATE_FMT")
	TIME=$($CMD_DATE +"$STD_TIME_FMT")
	read_heredoc ASK_TEXT <<EOF
The system date is set to:
${DATE}

The system time is set to:
${TIME}.

Do you want to change it?
EOF

	dialog_std \
		--title "Set date and time" \
		--yesno "$ASK_TEXT" \
		0 0
	case $? in
	1)
		exit
	;;
	*)
		exit 1
	;;
	esac

	while true; do
		dialog_read DATE \
			--title "Set date" \
			--calendar \
			0 0 0 0 0

		dialog_read TIME \
			--title "Set time" \
			--timebox "Current time (24 hour time)" \
			0 0

		DATETIME=$(convert_date "%d/%m/%Y %H:%M:%S" "$DATE $TIME")

		read_heredoc ASK_TEXT <<EOF
You have set the date and time to:

${DATETIME}

Does this look okay?
EOF
		dialog_std \
			--title "Set date and time" \
			--yesno "$ASK_TEXT" \
			0 0

		if [ $? -eq 0 ]; then
			config_date "%m/%d/%Y %H:%M:%S" "$DATE $TIME" \
				|| error "Could not set time"
		fi
	done
)

# setup_networking: Set up the network interface(s)
#
# Arguments:
# None
#
# Side effects:
# Configures network, writes network config
setup_networking ()
(
	dialog_read HOSTNAME \
		--title "System hostname" \
		--nocancel \
		--inputbox "Input your system hostname in FQDN format:" \
		0 0
	case $? in
	0)
		# no-op
	;;
	*)
		exit 1
	;;
	esac

	config_hostname || exit 1

	INTERFACES="$(get_interfaces || exit 1)"

	read_heredoc INTRO_TEXT <<EOF
We will now configure the network interfaces.

Select the ones to configure, or none to skip:
EOF

	if [ -z "$INTERFACES" ]; then
		dialog_std \
			--title "Configure network interfaces" \
			--msgbox "The system has no known interfaces to configure." \
			0 0
		exit 1
	fi

	# Build argument list
	ARGS=""
	for IFACE in $INTERFACES; do
		# Deconfigure if necessary, since we're here
		unlink "$TARGET/etc/init.d/net.$IFACE" &>/dev/null
		ARGS="$ARGS $IFACE $IFACE off"
	done

	SAVED=$(save_array "@")
	eval "set -- $ARGS"
	dialog_read INTERFACES_CFG \
		--title "Configure network interfaces" \
		--checklist "$INTRO_TEXT" \
		0 0 0 \
		"$@"
	eval "set -- $SAVED"

	# Any interfaces to configure?
	if [ -z "$INTERFACES_CFG" ]; then
		exit
	fi

	# Back up file
	if [ -f ${TARGET}/etc/conf.d/net ]; then
		cp "${TARGET}/etc/conf.d/net" "${TARGET}/etc/conf.d/net.backup"
	fi

	if [ ! -d ${TARGET}/etc/conf.d ]; then
		mkdir -p "${TARGET}/etc/conf.d"
	fi

	# Initalise file
	cat >"$TARGET/etc/conf.d/net" <<EOF
# This file was automatically generated by setup-adelie
# You may edit it to suit your needs.

EOF

	for IFACE in $INTERFACES_CFG; do
		dialog_std \
			--title "Configuring $IFACE" \
			--yesno "Do you want to use DHCP on $IFACE?" \
			0 0
		case $? in
		0)
			config_iface_dhcp $IFACE || exit 1
		;;
		1)
			# TODO input checking
			dialog_read IP_CFG \
				--title "Configuring $IFACE" \
				--nocancel \
				--inputbox "Input your IPs as a space-separated list:" \
				0 0
			case $? in 1|255) exit 1; ;; esac

			dialog_read ROUTE_CFG \
				--title "Configuring $IFACE" \
				--nocancel \
				--inputbox "Input your default route:" \
				0 0
			case $? in 1|255) exit 1; ;; esac

			dialog_read DNS_CFG \
				--title "Configuring $IFACE" \
				--nocancel \
				--inputbox "Input your DNS servers for this interface:" \
				0 0
			case $? in 1|255) exit 1; ;; esac

			dialog_read DNS_SEARCH_CFG \
				--title "Configuring $IFACE" \
				--nocancel \
				--inputbox "Input your search domain:" \
				0 0
			case $? in 1|255) exit 1; ;; esac

			config_iface_static $IFACE \
				"$IP_CFG" \
				"$ROUTE_CFG" \
				"$DNS_SEARCH_CFG" \
				"$DNS_CFG" || \
				exit 1
		;;
		*)
			dialog_std \
				--title "Interface configuration" \
				--msgbox "Not configuring $IFACE" \
				0 0
		;;
		esac
	done

	dialog_std \
		--title "Configuring networking" \
		--defaultno \
		--yesno "Do you want to do any manual configuration?" \
		0 0

	if [ $? -eq 0 ]; then
		$EDITOR "${TARGET}/etc/conf.d/net"
	fi
)

# setup_base: Set up the base system
#
# Arguments:
# None
#
# Side effects:
# Base system is made ready to use
#
# Notes:
# Depends on procfs etc. being mounted!
setup_base ()
(
	dialog_std \
		--title "Base system configuration" \
		--msgbox "We will now set up the Adélie base system." \
		0 0

	# XXX handle errors more gracefully
	(config_base || exit 1) | dialog_std \
		--title "Base system installation" \
		--programbox \
		15 60

	while true; do
		dialog_read PASSWORD \
			--title "Root user password" \
			--passwordbox "Input root password (nothing will be shown):" \
			0 0

		if [ -n "$PASSWORD" ]; then
			dialog_read PASSWORD2 \
				--title "Root user password" \
				--passwordbox "Input root password again:" \
				0 0

			if [ "x$PASSWORD" != "x$PASSWORD2" ]; then
				dialog_std \
					--msgbox "Passwords do not match." \
					0 0
				continue
			fi

			echo "root:$PASSWORD" | chpasswd -R "${TARGET}"
			if [ $? -eq 0 ]; then break; fi
		fi
	done

	dialog_std \
		--title "Base system configuration" \
		--yesno "Would you like to do any manual configuration?" \
		0 0
	case $? in
	0)
		clear
		echo "You are now being dropped to a shell."
		echo "Just exit when finished."
		chroot ${TARGET} /bin/sh
		clear
	;;
	1)
		exit
	;;
	*)
		exit 1
	;;
	esac
)

# setup_bootloader: Configure bootloader
#
# Arguments:
# None
#
# Side effects:
# Sets up the bootloader
setup_bootloader ()
(
	config_bootloader 
	case $? in
	0)
		# No-op
	;;
	2)
		read_heredoc <<EOF
Your architecture is not supported for bootloader install. Therefore, you will
have to install the bootloader by hand.
EOF
		dialog_std \
			--title "Bootloader installation" \
			--msgbox "$ERROR_TEXT" \
			0 0
	;;
	*)
		error "Bootloader installation failed"
	;;
	esac
)

# setup_closeout: Finish installation
#
# Arguments:
# None
#
# Side effects:
# May reboot the system
setup_closeout ()
(
	dialog_std \
		--title "Adélie Linux Installation" \
		--yesno "Installation complete. Do you wish to reboot?" \
		0 0
	clear
	if [ $? -eq 0 ]; then
		reboot
	fi
)

setup_intro		|| exit
setup_country		|| exit
setup_disks		|| do_exit
setup_timezone		|| do_exit
#setup_date_time		|| do_exit
setup_networking	|| do_exit
setup_base		|| do_exit
setup_bootloader	|| do_exit
setup_closeout
