.POSIX:

install:
	install -D -m755 usr/bin/setup-adelie.sh $(DESTDIR)/usr/bin/setup-adelie.sh
	ln -s setup-adelie.sh $(DESTDIR)/usr/bin/ainstall
	mkdir -p $(DESTDIR)/usr/lib/setup-adelie
	install -D -m644 -t $(DESTDIR)/usr/lib/setup-adelie usr/lib/setup-adelie/*
